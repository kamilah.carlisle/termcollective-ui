﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Drawing;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.Diagnostics;

/*+----------------------------------------------------------------------
 ||
 ||  Class Translate.xaml.cs 
 ||
 ||         Author:  Sukhmeet Khalar
 ||                  Kamilah Carlisle
 ||                  Ben Broadstone
 ||                  Alexander Vernet
 ||                  Kurtis Crowe
 ||                  Andrew Renteria
 ||
 ||        Purpose:  Retreive translation of the image user selected
 ||
 ||  Inherits From:  None
 ||
 ||
 |+-----------------------------------------------------------------------
 ||
 ||      Constants:  None
 ||
 |+-----------------------------------------------------------------------
 ||
 ||   Constructors:  userImage: Image data sent from MainPage
 ||
 ||  Class Methods:  RestService() - REST GET Call to backend
 ||
 ++-----------------------------------------------------------------------*/

namespace Transpic
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Translate : ContentPage
    {

        private Image image;
        private string imagePath;

        public Translate(String userImageInput)
        {
            image = new Image { Source = userImageInput };
            imagePath = userImageInput;
            InitializeComponent();
            
        }

        protected async override void OnAppearing()
        {
            //Debuggin Purpose to verify Image was passed along from MainPage
            //userImage.Source = image.Source;
            //await RestService_GET();
            await RestService_POST();
        }

        /*
         * REST Call for GET 
         */
        private async Task RestService_GET()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var request = await client.GetAsync(new Uri("http://192.168.1.8:8080/greeting"));

            if (request.IsSuccessStatusCode)
            {
                statusCode.Text += request.StatusCode.ToString();
                JObject translationResults = JObject.Parse(request.Content.ReadAsStringAsync().Result);
                translatedLang.Text += translationResults["content"];
                                 
            }
        }

        /*
         * REST Call for POST
         * URL is currently our actuator 
         * Currently the text is just for debugging
         * TODO: Extract single from JSON object
         */
        private async Task RestService_POST()
        {
            HttpClient client = new HttpClient();
            byte[] fileContents = ConvertImageToByteArray(imagePath);
            Uri webService = new Uri("http://192.168.1.8:8080/api/documents/translate/image");
            {
                try
                {
                    using (var content = new MultipartFormDataContent())
                    {
                        using (var memoryStream = new MemoryStream(fileContents))
                        {
                            using (var stream = new StreamContent(memoryStream))
                            {
                                content.Add(stream, "imageFile", "imageFile.jpeg");
                                content.Add(new StringContent("fr"), "startLanguage");
                                content.Add(new StringContent("en"), "targetLanguage");
                                using (var message = await client.PostAsync(webService, content))
                                {
                                    statusCode.Text = message.StatusCode.ToString();
                                    Debug.WriteLine("Content >>>> " + message.Content.ReadAsStringAsync().Result.ToString());
                                    Debug.WriteLine("Image >>>> " + stream.GetType());

                                }
                            }
                        }
                    }
                }
                catch (Exception e) {
                    Debug.WriteLine("Exception>>>>>>" + e);
                }
            }
        }

        //Step to convert the image to the Byte Array         
        public static byte[] ConvertImageToByteArray(string imagePath)
        {
            byte[] imageByteArray = null;
            FileStream fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
            using (BinaryReader reader = new BinaryReader(fileStream))
            {
                imageByteArray = new byte[reader.BaseStream.Length];
                for (int i = 0; i < reader.BaseStream.Length; i++)
                    imageByteArray[i] = reader.ReadByte();
            }
            return imageByteArray;
        }  
    }
}