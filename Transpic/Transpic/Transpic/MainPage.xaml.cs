﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.Net.Http;
using System.IO;
using System.Diagnostics;

/*+----------------------------------------------------------------------
 ||
 ||  Class MainPage.xaml.cs 
 ||
 ||         Author:  Sukhmeet Khalar
 ||                  Kamilah Carlisle
 ||                  Ben Broadstone
 ||                  Alexander Vernet
 ||                  Kurtis Crowe
 ||                  Andrew Renteria
 ||
 ||        Purpose:  This page is where the user will interact with the application
 ||                  User will be given the option to either take a picture or use one from their gallery
 ||                  The image will be displayed on the page and user must press the translate button to be sent to our backend service
 ||
 ||  Inherits From:  None
 ||
 |+-----------------------------------------------------------------------
 ||
 ||      Constants:  None
 ||
 |+-----------------------------------------------------------------------
 ||
 ||   Constructors:  N/A
 ||
 ||  Class Methods:  Take_Picture_Clicked(object sender, EventArgs e) - Button Eventlistener to allow user to take picture via their camera
 ||                  Upload_Picture_Clicked(object sender, EventArgs e) - Button Eventlistener to allow user to picture image from gallery
 ||                  Translate_Clicked(object sender, EventArgs e) - Button Eventlistener to send image to backend service and take user to translation
 ||
 ||
 ++-----------------------------------------------------------------------*/
namespace Transpic
{
    public partial class MainPage : ContentPage
    {
        //userImage stores the image selected/taken by the user to be passed on to Translate page to
        //to be translated
        private Image userImage;
        private string imagePath;
        public MainPage()
        {
            InitializeComponent();

        }

        /*
         * Method using Xamarin.Media plugin to allow user to use their devices
         * camera to take the picture they want translated
         */
        private async void Take_Picture_Clicked(object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DisplayAlert("No Camera", "GET AN NEW PHONE", "OK");
                return;
            }

            try
            {
                var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions {});
                if (file == null) { return; }
                userImage = new Image { Source = file.Path};
                imagePath = file.Path;
                Image1.Source = userImage.Source; /*ImageSource.FromStream(() => file.GetStream());*/
            }
            catch
            {
                await DisplayAlert("Error", "No image detected", "Ok");
            }
        }

        /*
         * Method using Xamarin.Media plugin to allow user to select image from
         * their gallery
         */
        private async void Upload_Picture_Clicked(object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await DisplayAlert("No Upload", "Selected a photo is not supported", "OK");
                return;
            }

            try
            {
                var file = await CrossMedia.Current.PickPhotoAsync();
                userImage = new Image { Source = file.Path };
                imagePath = file.Path;
                Image1.Source = ImageSource.FromStream(() => file.GetStream());
            }
            catch
            {
                await DisplayAlert("Error", "No image detected", "Ok");
            }


        }

        /*
         * Button Eventlistener created by Xamarin. The user will press this
         * button to send image to Translate page which contains the REST 
         * service. The Translate page has 1 parameter 
         */
        private void Translate_Clicked(object sender, EventArgs e)
        {
            if (srcLang.SelectedIndex == -1 || targetLang.SelectedIndex == -1)
            {
                DisplayAlert("Error", "Please select your languages.", "Ok");
                return;
            }

            if (userImage == null)
            {
                DisplayAlert("Error", "No Image Detected", "Okay");
                return;
            }
            this.Navigation.PushAsync(new Translate(imagePath));
        }
    }
}