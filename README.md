# Transpic UI
> Mobile application that transcribes and translate text from images

Transpic is a mobile application that will take a picture or allow user to import a picture from their gallery to be transcribe text using a tika server. Mobile application development is done using Xamarin, a crossplatform mobile application framework. The core development is done using Xamarin.Forms while certain native features will be pulled in using Xamarin.Android and Xamarin.iOS.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

* Visual Studio 2017 with Xamarin Package installed
* System running Mac OS X (Needed to build iOS application)


## Development setup

Step by step guide for setting up frontend dev:
https://skhalar.atlassian.net/wiki/spaces/TC/pages/3080211/Frontend+Project+Setup

## Installation
TODO

## Usage example

TODO



## Release History

N/A

## Team

* Sukhmeet Khalar – mithusingh32@gmail.com
* Alexander Vernet - alexanderjvernet@gmail.com
* Kamilah Carlisle - kamilah.carlisle@gmail.com
* Kurtis Crowe - kurtis.crowe@gmail.com
* Ben Broadstone - ben.broadstone@gmail.com
* Andrew Renteria - 	andrewrenteria93@gmail.com

TODO licensing 

## Contributing

1. Fork it (<https://github.com/yourname/yourproject/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
